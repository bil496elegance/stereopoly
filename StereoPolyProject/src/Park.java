
public class Park implements Kare {

	private int index=0;
	private int money;
	private String name;
	private boolean buyable;
	private String[] piyonlar;
	public Park(String name, String[] piyonlar) {
		super();
		this.name=name;
		this.money=0;
		this.buyable = false;
		this.piyonlar = piyonlar;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getPiyonlar() {
		return piyonlar;
	}
	public void setPiyonlar(String[] piyonlar) {
		this.piyonlar = piyonlar;
	}
	public boolean isBuyable() {
		return buyable;
	}
	public void payToPlayer(Oyuncu o){
		o.setPara(o.getPara()+money);
		this.money=0;
	}
	public void getFromPlayer(Oyuncu o, int payment){
		o.setPara(o.getPara()-payment);
		this.money+=payment;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	@Override
	public String getRenk() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean[] getOyuncuListesi() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setOyuncuListesi(boolean[] b) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setBuyable(boolean b) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setOwner(Oyuncu pawn) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int getFiyat() {
		// TODO Auto-generated method stub
		return 0;
	}
}