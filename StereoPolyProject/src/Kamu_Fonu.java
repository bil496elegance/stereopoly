
public class Kamu_Fonu implements Kare {
	private int index=0;
	private boolean buyable = false;
	private String[] piyonlar;
	private Kamu_Kart_Stack stack;
	private String name;
	public Kamu_Fonu(String name, String[] piyonlar, Kamu_Kart_Stack kks) {
		super();
		this.stack=kks;
		this.name=name;
		this.buyable = false;
		this.piyonlar = piyonlar;
	}
	public String[] getPiyonlar() {
		return piyonlar;
	}
	public void setPiyonlar(String[] piyonlar) {
		this.piyonlar = piyonlar;
	}
	public boolean isBuyable() {
		return buyable;
	}
	public Kamu_Karti pick(){
		return stack.pop();
	}	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	@Override
	public String getRenk() {
		// TODO Auto-generated method stub
		return null;
	}
	public Kamu_Kart_Stack getStack() {
		return stack;
	}
	public void setStack(Kamu_Kart_Stack stack) {
		this.stack = stack;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public boolean[] getOyuncuListesi() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setOyuncuListesi(boolean[] b) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setBuyable(boolean b) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setOwner(Oyuncu pawn) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int getFiyat() {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
}
