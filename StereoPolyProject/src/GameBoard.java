import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

import javazoom.jlme.util.Player;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import javax.swing.JScrollPane;

public class GameBoard {

	JTextArea historyField = new JTextArea();
	JTextField inputField;
	TTSEngine boardSpeech = new TTSEngine("cmu-slt-hsmm");
	TTSEngine boardSpeechde = new TTSEngine("bits1-hsmm");
	TTSEngine boardSpeechfr = new TTSEngine("enst-camille-hsmm");
	int inputMode = 0;
	String lastSpoken = "", lastSpoken_de = "", lastSpoken_fr = "";
	boolean wait_flag = false;
	private JFrame board_Frame;
	private JPanel board_Base;
	private Oyuncu o_1;
	private Oyuncu o_2;
	private Oyuncu o_3;
	private Oyuncu o_4;
	private int currentTurn = 1;
	private int parkMoney = 0;
	ArrayList<Kare> tiles;
	Sans_Kart_Stack sks;
	Kamu_Kart_Stack kks;
	private int playerNumber;
	private final JScrollPane scrollPane = new JScrollPane();
	private final String help = "H is for, Shortcut Overview. R is for, Replay Last Message. M is for, Map Overview. S is for, Asset Overview. "
			+ "1 is for, Accept. 2 is for, Refuse. "
			+ "Numbers through 1 to 4 can be used for homes and hotels. Please wait beep sound to enter input.";
	private final String help_de = "H ist f�r, Abk�rzung �bersicht. R ist f�r, Letzte Nachricht Wiederholen.M ist f�r, Karten�bersicht.S ist f�r, Verm�gens�bersicht."
			+ "1 ist f�r, Akzeptieren.2 ist f�r, Verweigern."
			+ "Zahlen bis 1 bis 4 k�nnen f�r H�user und Hotels genutzt werden." + " Bitte warten Sie den Signalton.";
	private final String help_fr = "H est pour, aper�u de raccourci. R est pour, Rejouer le dernier message. M est pour, Aper�u de la carte. S est pour, Aper�u de l'actif."
			+ "1 est pour, Accepter. 2 est pour, Refuser."
			+ "Les num�ros de 1 � 4 peuvent �tre utilis�s pour les maisons et les h�tels."+" Veuillez attendre un bip sonore pour entrer l'entr�e.";

	public int getCurrentTurn() {
		return currentTurn;
	}

	/**
	 * Create the frame.
	 * @throws InterruptedException 
	 */
	public GameBoard() throws InterruptedException {
		board_Frame = new JFrame("StereoPoly Local_MP");
		board_Frame.setBackground(new Color(51, 255, 204));
		board_Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		board_Frame.setBounds(100, 100, 529, 379);

		graphicalPanelCreation();

		board_Frame.setVisible(true);
		board_Frame.pack();
		board_Frame.setSize(529, 379);
		board_Frame.setResizable(false);

		inputField.requestFocusInWindow();
		inputField.setEnabled(false);

		createMapSkeleton();
		mapLayout();
	}

	/**
	 * Draw the components.*/
	private void graphicalPanelCreation() {
		inputField = new JTextField();
		board_Base = new JPanel();
		board_Base.setForeground(new Color(0, 0, 0));
		board_Base.setBackground(new Color(0, 204, 102));
		board_Base.setBorder(new EmptyBorder(5, 5, 5, 5));
		board_Frame.setContentPane(board_Base);
		board_Base.setLayout(null);
		board_Base.setFocusable(false);


		inputField.setFont(new Font("Calibri", Font.PLAIN, 11));
		inputField.setBounds(10, 320, 500, 20);
		board_Base.add(inputField);
		inputField.setColumns(10);
		DefaultCaret caret = (DefaultCaret)historyField.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		inputField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					if(inputField.getText().equalsIgnoreCase("1")) {
						inputMode = 1;
						wait_flag = false;
						inputField.setText("");
					}
					else if(inputField.getText().equalsIgnoreCase("2")) {
						inputMode = 2;
						wait_flag = false;
						inputField.setText("");
					}
					else if(inputField.getText().equalsIgnoreCase("3")) {
						inputMode = 3;
						wait_flag = false;
						inputField.setText("");
					}
					else if(inputField.getText().equalsIgnoreCase("4")) {
						inputMode = 4;
						wait_flag = false;
						inputField.setText("");
					}
					else if(inputField.getText().equalsIgnoreCase("s")) {
						if(currentTurn == 1 && !o_1.isIflas()) {
							assetOverview(o_1);
						}
						else if(currentTurn == 2 && !o_2.isIflas()) {
							assetOverview(o_2);
						}
						else if(o_3 != null && currentTurn == 3 && !o_3.isIflas()) {
							assetOverview(o_3);
						}
						else if(o_4 != null && currentTurn == 3 && !o_4.isIflas()) {
							assetOverview(o_4);
						}
						inputField.setText("");
					}
					else if(inputField.getText().equalsIgnoreCase("r")) {
						if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Go"))
							boardSpeech.say(lastSpoken);
						else if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Los"))
							boardSpeechde.say(lastSpoken_de);
						else if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("D�part"))
							boardSpeechfr.say(lastSpoken_fr);
						inputField.setText("");
					}
					else if(inputField.getText().equalsIgnoreCase("")) {
						wait_flag = false;
					}
					else if(inputField.getText().equalsIgnoreCase("h")) {
						if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
							boardSpeech.say(help);
						else if(tiles.get(0).getName().equals("Los"))
							boardSpeechde.say(help_de);
						else if(tiles.get(0).getName().equals("D�part"))
							boardSpeechfr.say(help_fr);

						inputField.setText("");
					}
					else if(inputField.getText().equalsIgnoreCase("m")) {
						if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
							boardSpeech.say(o_1.getPiyon() + " is at " + tiles.get(o_1.getKonum()).getName() + ".");
							boardSpeech.say(o_2.getPiyon() + " is at " + tiles.get(o_2.getKonum()).getName() + ".");}
						else if(tiles.get(0).getName().equals("Los")){
							boardSpeechde.say(o_1.getPiyon() + " ist bei " + tiles.get(o_1.getKonum()).getName() + ".");
							boardSpeechde.say(o_2.getPiyon() + " ist bei " + tiles.get(o_2.getKonum()).getName() + ".");}
						else if(tiles.get(0).getName().equals("D�part")){
							boardSpeechfr.say(o_1.getPiyon() + " est � " + tiles.get(o_1.getKonum()).getName() + ".");
							boardSpeechfr.say(o_2.getPiyon() + " est � " + tiles.get(o_2.getKonum()).getName() + ".");}

						if(o_3 != null)
							if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
								boardSpeech.say(o_3.getPiyon() + " is at " + tiles.get(o_3.getKonum()).getName() + ".");
							else if(tiles.get(0).getName().equals("Los"))
								boardSpeechde.say(o_3.getPiyon() + " ist bei " + tiles.get(o_3.getKonum()).getName() + ".");
							else if(tiles.get(0).getName().equals("D�part"))
								boardSpeechfr.say(o_3.getPiyon() + " est � " + tiles.get(o_3.getKonum()).getName() + ".");

						if(o_4 != null)
							if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
								boardSpeech.say(o_4.getPiyon() + " is at " + tiles.get(o_4.getKonum()).getName() + ".");
							else if(tiles.get(0).getName().equals("Los"))
								boardSpeechde.say(o_4.getPiyon() + " ist bei " + tiles.get(o_4.getKonum()).getName() + ".");
							else if(tiles.get(0).getName().equals("D�part"))
								boardSpeechfr.say(o_4.getPiyon() + " est � " + tiles.get(o_4.getKonum()).getName() + ".");

						inputField.setText("");
					}
					else {
						if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Go"))
							boardSpeech.say("You have entered wrong input.");
						else if(tiles.get(0).getName().equals("Los"))
							boardSpeechde.say("Sie haben falsche Eingabe eingegeben.");
						else if(tiles.get(0).getName().equals("D�part"))
							boardSpeechfr.say("Vous avez saisi une entr�e incorrecte.");
						inputField.setText("");
					}
				}

			}
		});
		board_Base.setFocusable(true);
		scrollPane.setBounds(10, 11, 500, 300);

		board_Base.add(scrollPane);
		scrollPane.setViewportView(historyField);


		historyField.setEditable(false);
		historyField.setBackground(new Color(255, 255, 224));
		historyField.setFont(new Font("Calibri", Font.PLAIN, 14));
		historyField.setFocusable(true);

		inputField.requestFocusInWindow();

	}

	/**
	 * Create the tiles to use for parallel coordination.
	 * @throws InterruptedException 
	 * */
	private void createMapSkeleton() {

		tiles = new ArrayList<Kare>(40);
		Kare start = new Baslangic();
		Kare brown1 = new Arsa("Brown 1", "Brown", 60, null, 0, false, 50, 50, false, 2);
		Kare c1 = new Kamu_Fonu("Community Chest", new String[4], kks);
		Kare brown2 = new Arsa("Brown 2", "Brown", 60, null, 0, false, 50, 50, false, 4);
		Kare incTax = new Vergi("Income Tax", 200, new String[4]);
		Kare st_south = new Istasyon("South Station", new String[4], 200, 25);
		Kare cyan1 = new Arsa("Cyan 1", "Cyan", 80, null, 0, false, 50, 50, false, 6);
		Kare ch1 = new Sans("Chance", new String[4], sks);
		Kare cyan2 = new Arsa("Cyan 2", "Cyan", 100, null, 0, false, 50, 50, false, 6);
		Kare cyan3 = new Arsa("Cyan 3", "Cyan", 120, null, 0, false, 50, 50, false, 8);
		Kare hapis = new Ziyaretci("Ziyaret", new String[4]);
		Kare magenta1 = new Arsa("Mag 1", "Magenta", 140, null, 0, false, 100, 100, false, 10);
		Kare electricCorp = new Idare("Electric", new String[4], 150, null, 4);
		Kare magenta2 = new Arsa("Mag 2", "Magenta", 140, null, 0, false, 100, 100, false, 10);
		Kare magenta3 = new Arsa("Mag 3", "Magenta", 160, null, 0, false, 100, 100, false, 12);
		Kare st_west = new Istasyon("West Station", new String[4], 200, 25);
		Kare orange1 = new Arsa("Orange 1", "Orange", 180, null, 0, false, 100, 100, false, 14);
		Kare c2 = new Kamu_Fonu("Community Chest", new String[4], kks);
		Kare orange2 = new Arsa("Orange 2", "Orange", 180, null, 0, false, 100, 100, false, 14);
		Kare orange3 = new Arsa("Orange 3", "Orange", 200, null, 0, false, 100, 100, false, 16);
		Kare carpark = new Park("Auto Park", new String[4]);
		Kare red1 = new Arsa("Red 1", "Red", 220, null, 0, false, 150, 150, false, 18);
		Kare ch2 = new Sans("Chance", new String[4], sks);
		Kare red2 = new Arsa("Red 2", "Red", 220, null, 0, false, 150, 150, false, 18);
		Kare red3 = new Arsa("Red 3", "Red", 240, null, 0, false, 150, 150, false, 20);
		Kare st_north = new Istasyon("North Station", new String[4], 200, 25);
		Kare yellow1 = new Arsa("Yellow 1", "Yellow", 260, null, 0, false, 150, 150, false, 22);
		Kare yellow2 = new Arsa("Yellow 2", "Yellow", 260, null, 0, false, 150, 150, false, 22);
		Kare waterCorp = new Idare("Waterworks", new String[4], 150, null, 4);
		Kare yellow3 = new Arsa("Yellow 3", "Yellow", 280, null, 0, false, 150, 150, false, 24);
		Kare hapisAct = new Hapis("Kodes", new String[4]);
		Kare green1 = new Arsa("Green 1", "Green", 300, null, 0, false, 200, 200, false, 26);
		Kare green2 = new Arsa("Green 2", "Green", 300, null, 0, false, 200, 200, false, 26);
		Kare c3 = new Kamu_Fonu("Community Chest", new String[4], kks);
		Kare green3 = new Arsa("Green 3", "Green", 320, null, 0, false, 200, 200, false, 28);
		Kare st_east = new Istasyon("East Station", new String[4], 200, 25);
		Kare ch3 = new Sans("Chance", new String[4], sks);
		Kare blue1 = new Arsa("Blue 1", "Blue", 350, null, 0, false, 200, 200, false, 35);
		Kare luxTax = new Vergi("Luxury Tax", 100, new String[4]);
		Kare blue2 = new Arsa("Blue 2", "Blue", 400, null, 0, false, 200, 200, false, 50);

		tiles.add(start);
		tiles.add(brown1);
		tiles.add(c1);
		tiles.add(brown2);
		tiles.add(incTax);
		tiles.add(st_south);
		tiles.add(cyan1);
		tiles.add(ch1);
		tiles.add(cyan2);
		tiles.add(cyan3);
		tiles.add(hapis);
		tiles.add(magenta1);
		tiles.add(electricCorp);
		tiles.add(magenta2);
		tiles.add(magenta3);
		tiles.add(st_west);
		tiles.add(orange1);
		tiles.add(c2);
		tiles.add(orange2);
		tiles.add(orange3);
		tiles.add(carpark);
		tiles.add(red1);
		tiles.add(ch2);
		tiles.add(red2);
		tiles.add(red3);
		tiles.add(st_north);
		tiles.add(yellow1);
		tiles.add(yellow2);
		tiles.add(waterCorp);
		tiles.add(yellow3);
		tiles.add(hapisAct);
		tiles.add(green1);
		tiles.add(green2);
		tiles.add(c3);
		tiles.add(green3);
		tiles.add(st_east);
		tiles.add(ch3);
		tiles.add(blue1);
		tiles.add(luxTax);
		tiles.add(blue2);

	}

	private void mapLayout() throws InterruptedException {
		boardSpeech.say("Press 1 for U.S.A. Edition.");
		historyField.append("Press 1 for U.S.A. Edition.\n");
		Thread.sleep(800);
		boardSpeech.say("Press 2 for Web Lovers Edition.");
		historyField.append("Press 2 for Web Lovers Edition.\n");
		Thread.sleep(800);
		boardSpeech.say("Press 3 for German Edition.");
		historyField.append("Press 3 for German Edition.\n");
		Thread.sleep(800);
		boardSpeech.say("Press 4 for French Edition.");
		historyField.append("Press 4 for French Edition.\n");
		Thread.sleep(800);
		soundEffects("effects\\beep.mp3");
		inputField.setEnabled(true);
		inputField.requestFocusInWindow();
		wait_flag = true;
		while(wait_flag) {
			Thread.sleep(100);
		}
		inputField.setEnabled(false);
		int p = inputMode;
		String choice = "";

		if(p == 1) {
			choice = "UsaEdition";
			historyField.append("Usa Edition.\n");
		}
		else if(p == 2) {
			choice = "WebLoversEdition";
			historyField.append("Web Lovers Edition.\n");
		}
		else if(p == 3) {
			choice = "GermanEdition";
			historyField.append("German Edition.\n");
		}
		else if(p == 4) {
			choice = "FrenchEdition";
			historyField.append("French Edition.\n");
		}
		else {
			choice = "UsaEdition";
			historyField.append("Usa Edition.\n");
		}

		historyField.setText("");

		JSonMapParser mapParser = new JSonMapParser();
		ArrayList<String> names = mapParser.jsonParser(choice);

		for(int i = 0; i < tiles.size(); i++) {
			tiles.get(i).setName(names.get(i));
		}

		sks = new Sans_Kart_Stack();
		kks = new Kamu_Kart_Stack();

		if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Go")) {
			kks.push(new Bankaya_Para_Veren_Kamu_Karti(50,"Insurence policy has changed. Pay 50 dollars."));
			kks.push(new Ileri_Kare_Giden_Kamu_Karti(tiles.get(0),"Go to Start."));
			kks.push(new Geri_Kare_Giden_Kamu_Karti(tiles.get(1),"Go back to " + tiles.get(1).getName()));
			kks.push(new Bankadan_Para_Alan_Kamu_Karti(200,"There was a mistake in your bank account! Take 200 dollars."));
			kks.push(new Ileri_Giden_Kamu_Karti(3,"Move 3 tiles forward."));
			kks.push(new Geri_Giden_Kamu_Karti(2,"Move 2 tiles backward"));
			sks.push(new Bankaya_Para_Veren_Sans_Karti(100,"Income Tax, 100 dollars"));
			sks.push(new Ileri_Kare_Giden_Sans_Karti(tiles.get(16),"Go to " + tiles.get(16).getName()));
			sks.push(new Geri_Kare_Giden_Sans_Karti(tiles.get(25),"Go back to South Station."));
			sks.push(new Bankadan_Para_Alan_Sans_Karti(100,"Bank pays 100 dollars for interest share."));
			sks.push(new Ileri_Giden_Sans_Karti(5,"Move 5 tiles forward."));
			sks.push(new Geri_Giden_Sans_Karti(4,"Move 4 tiles backward"));
		}

		else if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Los")){
			kks.push(new Bankaya_Para_Veren_Kamu_Karti(50,"Versicherungspolice hat sich ge�ndert. Zahlen 50 Dollar."));
			kks.push(new Ileri_Kare_Giden_Kamu_Karti(tiles.get(0),"Gehen Sie zu Start."));
			kks.push(new Geri_Kare_Giden_Kamu_Karti(tiles.get(1),"Geh zur�ck zu " + tiles.get(1).getName()));
			kks.push(new Bankadan_Para_Alan_Kamu_Karti(200,"Es gab einen Fehler auf deinem Bankkonto! Nehmen Sie 200 Dollar."));
			kks.push(new Ileri_Giden_Kamu_Karti(3,"Bewege 3 Fliesen nach vorne."));
			kks.push(new Geri_Giden_Kamu_Karti(2,"Bewege 2 Fliesen nach hinten"));
			sks.push(new Bankaya_Para_Veren_Sans_Karti(100,"Einkommensteuer, 100 Dollar."));
			sks.push(new Ileri_Kare_Giden_Sans_Karti(tiles.get(16),"Geh zur�ck zu " + tiles.get(16).getName()));
			sks.push(new Geri_Kare_Giden_Sans_Karti(tiles.get(25),"Geh zur�ck zum S�dbahnhof."));
			sks.push(new Bankadan_Para_Alan_Sans_Karti(100,"Bank zahlt 100 euros f�r Zinsanteil."));
			sks.push(new Ileri_Giden_Sans_Karti(5,"Bewege 5 Fliesen nach vorne."));
			sks.push(new Geri_Giden_Sans_Karti(4,"Bewege 4 Fliesen nach hinten"));
		}
		else if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("D�part")){
			kks.push(new Bankaya_Para_Veren_Kamu_Karti(50,"La politique d'assurance a chang�. Payer 50 dollars."));
			kks.push(new Ileri_Kare_Giden_Kamu_Karti(tiles.get(0),"Aller � d�part."));
			kks.push(new Geri_Kare_Giden_Kamu_Karti(tiles.get(1),"Revenir � " + tiles.get(1).getName()));
			kks.push(new Bankadan_Para_Alan_Kamu_Karti(200,"Une erreur s'est produite dans votre compte bancaire! Prenez 200 dollars."));
			kks.push(new Ileri_Giden_Kamu_Karti(3,"D�placez 3 tuiles vers l'avant."));
			kks.push(new Geri_Giden_Kamu_Karti(2,"D�placer 2 tuiles vers l'arri�re."));
			sks.push(new Bankaya_Para_Veren_Sans_Karti(100,"Imp�t sur le revenu, 100 dollars"));
			sks.push(new Ileri_Kare_Giden_Sans_Karti(tiles.get(16),"Aller � " + tiles.get(16).getName()));
			sks.push(new Geri_Kare_Giden_Sans_Karti(tiles.get(25),"Retournez � South Station."));
			sks.push(new Bankadan_Para_Alan_Sans_Karti(100,"La banque paie 100 euros pour la part d'int�r�t."));
			sks.push(new Ileri_Giden_Sans_Karti(5,"D�placez 5 tuiles vers l'avant."));
			sks.push(new Geri_Giden_Sans_Karti(4,"D�placer 4 tuiles vers l'arri�re."));
		}

		((Kamu_Fonu) tiles.get(2)).setStack(kks);
		((Kamu_Fonu) tiles.get(17)).setStack(kks);
		((Kamu_Fonu) tiles.get(33)).setStack(kks);

		((Sans) tiles.get(7)).setStack(sks);
		((Sans) tiles.get(22)).setStack(sks);
		((Sans) tiles.get(36)).setStack(sks);

	}

	/**
	 * Sign the players to the game. Up to 4 is supported.
	 * */
	private void signPlayers(int number) {
		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
			o_1 = createPlayer("Car");
			o_2 = createPlayer("Ship");
			o_1.setOyuncu_no(1);
			o_2.setOyuncu_no(2);
			if(number > 2) {
				o_3 = createPlayer("Bicycle");
				o_3.setOyuncu_no(3);
			}
			if(number > 3) {
				o_4 = createPlayer("Plane");
				o_4.setOyuncu_no(4);
			}

			playerNumber = number;                                       

		}


		else if(tiles.get(0).getName().equals("Los")){
			o_1 = createPlayer("Auto");
			o_2 = createPlayer("Schiff");
			o_1.setOyuncu_no(1);
			o_2.setOyuncu_no(2);
			if(number > 2) {
				o_3 = createPlayer("Fahrrad");
				o_3.setOyuncu_no(3);
			}
			if(number > 3) {
				o_4 = createPlayer("Ebene");
				o_4.setOyuncu_no(4);
			}

			playerNumber = number;     
		}

		else if(tiles.get(0).getName().equals("D�part")){
			o_1 = createPlayer("Voiture");
			o_2 = createPlayer("Navire");
			o_1.setOyuncu_no(1);
			o_2.setOyuncu_no(2);
			if(number > 2) {
				o_3 = createPlayer("V�lo");
				o_3.setOyuncu_no(3);
			}
			if(number > 3) {
				o_4 = createPlayer("Avion");
				o_4.setOyuncu_no(4);
			}

			playerNumber = number;     
		}
	}

	/**
	 * Create the player with selected pawn.
	 * */
	private Oyuncu createPlayer(String pawn) {
		return new Oyuncu(pawn);
	}

	/**
	 * Make players roll. Sort them out according to the rolls. They gain turns on it.
	 * This method is in progress.
	 * */
	private int[] initiateTurnCheck() {
		int[] zarlar = {-1, -1, -1, -1};
		int[] s�ra = {1, 2, 3, 4};

		if(o_1 != null) {
			zarlar[0] = o_1.zarAt();
		}
		if(o_2 != null) {
			zarlar[1] = o_2.zarAt();
		}	
		if(o_3 != null) {
			zarlar[2] = o_3.zarAt();
		}
		if(o_4 != null) {
			zarlar[3] = o_4.zarAt();
		}

		for(int i = 0; i < 4; i++) {
			int min = i;
			for(int j = i + 1; j < 4; j++) {
				if(zarlar[j] < zarlar[min])
					min = j;

			}
			if(min != i) {
				int zarTemp = zarlar[i];
				int oTemp = s�ra[i];

				zarlar[i] = zarlar[min];
				s�ra[i] = s�ra[min];

				zarlar[min] = zarTemp;
				s�ra[min] = oTemp;
			}
		}
		return s�ra;
	}

	/**
	 * Make players play their turns. Currently in progress, but handles basic movement.
	 * @throws InterruptedException 
	 * */
	public void playTurn() throws InterruptedException {
		if(playerNumber != 1) {

			if(currentTurn > playerNumber)
				currentTurn = 1;

			if(currentTurn == 1) {
				playPlayerTurn(o_1, "effects\\carTurn.mp3");
			}
			else if(currentTurn == 2) {
				playPlayerTurn(o_2, "effects\\ship.mp3");
			}
			else if(currentTurn == 3) {
				playPlayerTurn(o_3, "effects\\bicycle.mp3");
			}
			else if(currentTurn == 4) {
				playPlayerTurn(o_4, "effects\\airplane.mp3");
			}
		}
		else {
			if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Go")){
				boardSpeech.say("We have a winner!");
				historyField.append("We have a winner!");
			}
			else if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Los")){
				boardSpeechde.say("wir haben einen Sieger!");
				historyField.append("wir haben einen Sieger!");
			}
			else if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("D�part")){
				boardSpeechfr.say("nous avons un gagnant!");
				historyField.append("nous avons un gagnant!");
			}



			Thread.sleep(700);
			if(!o_1.isIflas()) {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("Car has won the game with " + o_1.getPara());
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Auto hat das Spiel gewonnen mit " + o_1.getPara());
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Voiture a remport� le match avec" + o_1.getPara());


			}
			else if(!o_2.isIflas()) {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("Ship has won the game with " + o_2.getPara());
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Schiff hat das Spiel gewonnen mit  " + o_2.getPara());
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Navire a remport� le match avec " + o_2.getPara());

			}
			else if(o_3 != null && !o_3.isIflas()) {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("Bicycle has won the game with " + o_3.getPara());
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Fahrrad hat das Spiel gewonnen mit  " + o_2.getPara());
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Haut-de-forme a remport� le match avec " + o_2.getPara());


			}
			else if(o_4 != null && !o_4.isIflas()) {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("Plane has won the game with " + o_4.getPara());
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Ebene hat das Spiel mit gewonnen " + o_4.getPara());
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Avion a remport� le match avec " + o_4.getPara());


			}
			soundEffects("effects\\clap.mp3");
			Thread.sleep(1000);
			System.exit(1);
		}

	}

	public void welcome() throws InterruptedException {
		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			boardSpeech.say("Welcome to the StereoPoly. How many players will play?");
		else if(tiles.get(0).getName().equals("Los"))
			boardSpeechde.say("Willkommen im StereoPoly. Wie viele Spieler werden spielen?");
		else if(tiles.get(0).getName().equals("D�part"))
			boardSpeechfr.say("Bienvenue � la StereoPoly. Combien de joueurs vont jouer?");
		soundEffects("effects\\beep.mp3");
		inputField.setEnabled(true);
		inputField.requestFocusInWindow();
		wait_flag = true;
		while(wait_flag) {
			Thread.sleep(100);
		}
		inputField.setEnabled(false);
		int players = inputMode;

		signPlayers(players);
		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			boardSpeech.say("Please roll your dice to determine your turn!");
		else if(tiles.get(0).getName().equals("Los"))
			boardSpeechde.say("Bitte rollen Sie Ihre W�rfel, um Ihren Zug zu bestimmen!");
		else if(tiles.get(0).getName().equals("D�part"))
			boardSpeechfr.say("S'il vous pla�t rouler vos d�s pour d�terminer votre tour!");


		initiateTurnCheck();
		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			boardSpeech.say(help);
		else if(tiles.get(0).getName().equals("Los"))
			boardSpeechde.say(help_de);
		else if(tiles.get(0).getName().equals("D�part"))
			boardSpeechfr.say(help_fr);

		inputField.setText("");
	}

	private static void soundEffects(String folderpath){
		try{
			FileInputStream fis = new FileInputStream(folderpath);
			Player playMP3 = new Player(fis);

			playMP3.play();
		}
		catch(Exception e)
		{
			System.out.println(e);

		}

	}

	private void playPlayerTurn(Oyuncu o, String voiceEffectPath) throws InterruptedException {
		int d = 0;
		int prevIndex;
		int index;

		if(o == null)
			return;

		if(o.isIflas())
			return;

		if(o.isKodes()) {
			if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
				boardSpeech.say("You are in Jail. You can use your card, pay your fee, or roll a double to bail out of jail.");
			else if(tiles.get(0).getName().equals("Los"))
				boardSpeechde.say("Du bist im Gef�ngnis. Sie k�nnen Ihre Karte verwenden, Ihre Geb�hr bezahlen, oder rollen Sie ein Doppel, um aus dem Gef�ngnis zu retten.");
			else if(tiles.get(0).getName().equals("D�part"))
				boardSpeechfr.say("Vous �tes en prison. Vous pouvez utiliser votre carte, payer vos honoraires, ou rouler un double pour sortir de prison.");


			if(o.getKodesKart�()) {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("You have a \"Get out of Jail\" card. Press 1 to use it.");
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Du hast ein \" Holen Sie sich aus dem Gef�ngnis\" karte. Dr�cken Sie 1, um es zu benutzen.");
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Tu as un \"Sortez de la prison\" carte. Appuyez sur 1 pour l'utiliser. ");
			}
			else {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("You don't have a card.");
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Du hast keine Karte.");
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Vous n'avez pas de carte.");
			}
			if(o.getPara() > 5) {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("You have enough money to bail out. Press 2 to bail out.");
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Du hast genug Geld, um zu reparieren. Dr�cken Sie 2, um zu reparieren.");
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Vous avez assez d'argent pour sauver. Appuyez sur 2 pour sortir.");
			}
			else {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("You don't have enough money to bail out.");
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Du hast nicht genug Geld, um es zu retten.");
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Vous n'avez pas assez d'argent pour sauver.");
			}
			if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
				boardSpeech.say("Press 3 if you want to try your luck.");
			else if(tiles.get(0).getName().equals("Los"))
				boardSpeechde.say("Dr�cken Sie 3, wenn Sie Ihr Gl�ck ausprobieren m�chten.");
			else if(tiles.get(0).getName().equals("D�part"))
				boardSpeechfr.say("Appuyez sur 3 si vous voulez essayer votre chance.");


			wait_flag = true;
			inputMode = 0;
			inputField.setEnabled(true);
			inputField.requestFocusInWindow();
			soundEffects("effects\\beep.mp3");
			while(wait_flag) {
				Thread.sleep(100);
			}
			inputField.setEnabled(false);
			if(inputMode == 1) {
				o.setKodesKart�(false);
				o.setKodes(false);
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					boardSpeech.say("You bailed out of the jail by using your card.");
				else if(tiles.get(0).getName().equals("Los"))
					boardSpeechde.say("Sie haben aus dem Gef�ngnis gerettet, indem Sie Ihre Karte benutzen.");
				else if(tiles.get(0).getName().equals("D�part"))
					boardSpeechfr.say("Vous avez renflou� de la prison en utilisant votre carte.");
			}
			else if(inputMode == 2) {
				o.setPara(o.getPara() - 50);
				o.setKodes(false);
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")) {
					boardSpeech.say("You bailed out of the jail by paying 50 dollars!");
					Thread.sleep(700);
					historyField.append("You bailed out of the jail by paying 50 dollars!\n");
				}
				else if(tiles.get(0).getName().equals("Los")) {
					boardSpeechde.say("Sie haben aus dem Gef�ngnis gerettet, indem sie 50 Euros bezahlen!");
					Thread.sleep(700);
					historyField.append("Sie haben aus dem Gef�ngnis gerettet, indem sie 50 Euros bezahlen!\n");
				}
				else if(tiles.get(0).getName().equals("D�part")) {
					boardSpeechfr.say("Vous avez renflou� en prison en payant 50 euros!");
					Thread.sleep(700);
					historyField.append("Vous avez renflou� en prison en payant 50 euros!\n");
				}

			}
			else {
				d = o.zarAt();
				if(o.isKodes()) {
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
					{
						boardSpeech.say("You failed to roll a pair. You are still in the jail.");
						boardSpeech.say("Press enter to end your turn.");}
					else if(tiles.get(0).getName().equals("Los")){
						boardSpeechde.say("Du hast es nicht geschafft, ein Paar zu rollen. Du bist immer noch im Gef�ngnis.");
						boardSpeechde.say("Dr�cken Sie die Eingabetaste, um den Zug zu beenden.");}
					else if(tiles.get(0).getName().equals("D�part")){
						boardSpeechfr.say("Vous n'avez pas r�ussi � rouler une paire. Vous �tes toujours dans la prison.");
						boardSpeechfr.say("Appuyez sur Entr�e pour terminer votre tour.");}

					return;
				}
				else {
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
						boardSpeech.say("You rolled " + d + ". You are out of jail.");
					else if(tiles.get(0).getName().equals("Los"))
						boardSpeechde.say("Sie rollten" + d + ". Du bist aus dem Gef�ngnis.");
					else if(tiles.get(0).getName().equals("D�part"))
						boardSpeechfr.say("Vous avez roul�" + d + ". Vous �tes hors de prison.");

					return;
				}
			}


		}

		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			boardSpeech.say("It's " + o.getPiyon() + "'s turn. Press enter to roll your dice.");
		else if(tiles.get(0).getName().equals("Los"))
			boardSpeechde.say("Es ist " + o.getPiyon() + "an der reihe.Dr�cken Sie Enter, um Ihre W�rfel zu rollen.");
		else if(tiles.get(0).getName().equals("D�part"))
			boardSpeechfr.say("c'est le tour de la " + o.getPiyon() + ".Appuyez sur Entr�e pour lancer vos d�s.");	

		Thread.sleep(800);

		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			historyField.append("It's " + o.getPiyon() + "'s turn. Press enter to roll your dice.\n");
		else if(tiles.get(0).getName().equals("Los"))
			historyField.append("Es ist " + o.getPiyon() + "an der reihe.Dr�cken Sie Enter, um Ihre W�rfel zu rollen.\n");
		else if(tiles.get(0).getName().equals("D�part"))
			historyField.append("c'est le tour de la " + o.getPiyon() + ".Appuyez sur Entr�e pour lancer vos d�s.\n");

		wait_flag = true;
		soundEffects("effects\\beep.mp3");
		inputField.setEnabled(true);
		inputField.requestFocusInWindow();
		while(wait_flag) {
			Thread.sleep(100);
		}
		inputField.setEnabled(false);

		prevIndex = o.getKonum();
		d = o.zarAt();
		if(d == -1) {
			o.git_Geri_Kare(tiles.get(10));
			o.setKodes(true);
		}
		soundEffects("effects\\zar.mp3");

		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			boardSpeech.say(o.getPiyon() + " rolled " + d + " .");
		else if(tiles.get(0).getName().equals("Los"))
			boardSpeechde.say(o.getPiyon() + " gerollt " + d + " .");
		else if(tiles.get(0).getName().equals("D�part"))
			boardSpeechfr.say(o.getPiyon() + " roul�e " + d + " .");

		Thread.sleep(800);
		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			historyField.append(o.getPiyon() + " rolled " + d + ".\n");
		else if(tiles.get(0).getName().equals("Los"))
			historyField.append(o.getPiyon() + " gerollt " + d + ".\n");
		else if(tiles.get(0).getName().equals("D�part"))
			historyField.append(o.getPiyon() + " roul�e " + d + ".\n");

		o.git_Ileri(d);
		index = o.getKonum();


		soundEffects(voiceEffectPath);
		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			boardSpeech.say(o.getPiyon() + " is at " + tiles.get(o.getKonum()).getName() + ".");
		else if(tiles.get(0).getName().equals("Los"))
			boardSpeechde.say(o.getPiyon() + " ist bei " + tiles.get(o.getKonum()).getName() + ".");
		else if(tiles.get(0).getName().equals("D�part"))
			boardSpeechfr.say(o.getPiyon() + " est � " + tiles.get(o.getKonum()).getName() + ".");


		Thread.sleep(800);
		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			historyField.append(o.getPiyon() + " is at " + tiles.get(o.getKonum()).getName() + ".\n");
		else if(tiles.get(0).getName().equals("Los"))
			historyField.append(o.getPiyon() + " ist bei " + tiles.get(o.getKonum()).getName() + ".\n");
		else if(tiles.get(0).getName().equals("D�part"))
			historyField.append(o.getPiyon() + " est � " + tiles.get(o.getKonum()).getName() + ".\n");

		if(index < prevIndex) {
			if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
				boardSpeech.say(o.getPiyon() + " has passed through start. Collect 200 dollars!");
				historyField.append(o.getPiyon() + " has passed through start. Collect 200 dollars!\n");}
			else if(tiles.get(0).getName().equals("Los")){
				boardSpeechde.say(o.getPiyon() + " Hat den Start durchlaufen. Sammle 200 euros!");
				historyField.append(o.getPiyon() + "Hat den Start durchlaufen. Sammle 200 euros!\n");}
			else if(tiles.get(0).getName().equals("D�part")){
				boardSpeechfr.say(o.getPiyon() + " A pass� par le d�but. Recueillir 200 euros!");
				historyField.append(o.getPiyon() + "  A pass� par le d�but. Recueillir 200 euros!\n");}
			Thread.sleep(800);
		}

		if(tiles.get(index).isBuyable()) {

			if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
				boardSpeech.say("You have " + o.getPara() + " dollars!");
			else if(tiles.get(0).getName().equals("Los"))
				boardSpeechde.say("Du hast " + o.getPara() + " euros!");
			else if(tiles.get(0).getName().equals("D�part"))
				boardSpeechfr.say("Tu as " + o.getPara() + " euros!");

			Thread.sleep(800);

			if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
				historyField.append("You have " + o.getPara() + " dollars!\n");
				boardSpeech.say("Do you want to buy " + tiles.get(index).getName() + " for " + tiles.get(index).getFiyat() + " dollars?");
			}
			else if(tiles.get(0).getName().equals("Los")){
				historyField.append("Du hast " + o.getPara() + " euros!\n");
				boardSpeechde.say("M�chten sie kaufen " + tiles.get(index).getName() + " f�r " + tiles.get(index).getFiyat() + " euros?");
			}
			else if(tiles.get(0).getName().equals("D�part")){
				historyField.append("Tu as" + o.getPara() + " euros!\n");
				boardSpeechfr.say("Veux-tu acheter " + tiles.get(index).getName() + " pour " + tiles.get(index).getFiyat() + " euros?");
			}

			Thread.sleep(800);

			if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
				historyField.append("Do you want to buy " + tiles.get(index).getName() + " for " + tiles.get(index).getFiyat() + " dollars?\n");
			else if(tiles.get(0).getName().equals("Los"))
				historyField.append("M�chten sie kaufen " + tiles.get(index).getName() + " for " + tiles.get(index).getFiyat() + " euros?\n");
			else if(tiles.get(0).getName().equals("D�part"))
				historyField.append("Veux-tu acheter " + tiles.get(index).getName() + " for " + tiles.get(index).getFiyat() + " euros?\n");

			lastSpoken = "You have rolled "+ d +". You have " + o.getPara() + " dollars!" + 
					"Do you want to buy " + tiles.get(index).getName() + " for " + tiles.get(index).getFiyat() + " dollars?";
			lastSpoken_de = "Du wurdest gerollt "+ d +". Du hast" + o.getPara() + " euros!" + 
					"M�chten sie kaufen" + tiles.get(index).getName() + " f�r " + tiles.get(index).getFiyat() + " euros?";
			lastSpoken_fr = "Tu as roul� "+ d +". Du hast" + o.getPara() + " euros!" + 
					"Veux-tu acheter" + tiles.get(index).getName() + " pour " + tiles.get(index).getFiyat() + " euros?";

			inputField.setEnabled(true);
			inputField.requestFocusInWindow();
			wait_flag = true;
			inputMode = 0;
			while(wait_flag) {
				Thread.sleep(100);
			}
			inputField.setEnabled(false);
			int choice = inputMode;
			if(choice == 1) {
				if(o.getPara() < tiles.get(index).getFiyat()) {
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
						boardSpeech.say("You don't have enough money, You can not afford this place.");
						historyField.append("You don't have enough money, You can not afford this place.\n");
					}
					else if(tiles.get(0).getName().equals("Los")){
						boardSpeechde.say("Sie haben nicht genug Geld, Sie k�nnen sich diesen Platz nicht leisten.");
						historyField.append("Sie haben nicht genug Geld, Sie k�nnen sich diesen Platz nicht leisten.\n");
					}
					else if(tiles.get(0).getName().equals("D�part")){
						boardSpeechfr.say("Vous n'avez pas assez d'argent, Vous ne pouvez pas vous permettre cet endroit.");
						historyField.append("Vous n'avez pas assez d'argent, Vous ne pouvez pas vous permettre cet endroit.\n");
					}
				}
				else {
					o.sat�nAl(tiles.get(index));

					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
						boardSpeech.say("You have bought " + tiles.get(index).getName() + " for " + tiles.get(index).getFiyat() + " dollars!");
					else if(tiles.get(0).getName().equals("Los"))
						boardSpeechde.say("Du hast gekauft " + tiles.get(index).getName() + " f�r " + tiles.get(index).getFiyat() + " euros!");
					else if(tiles.get(0).getName().equals("D�part"))
						boardSpeechfr.say("Vous avez achet� " + tiles.get(index).getName() + " pour " + tiles.get(index).getFiyat() + " euros!");

					Thread.sleep(800);

					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
						historyField.append("You have bought " + tiles.get(index).getName() + " for " + tiles.get(index).getFiyat() + " dollars!\n");
						boardSpeech.say("You have " + o.getPara() + " dollars left!");}
					else if(tiles.get(0).getName().equals("Los")){
						historyField.append("Du hast gekauft " + tiles.get(index).getName() + " f�r " + tiles.get(index).getFiyat() + " euros!\n");
						boardSpeechde.say("Sie haben " + o.getPara() + " euros �brig!");}
					else if(tiles.get(0).getName().equals("D�part")){
						historyField.append("Vous avez achet� " + tiles.get(index).getName() + " pour " + tiles.get(index).getFiyat() + " euros!\n");
						boardSpeechfr.say("Vous avez encore " + o.getPara() + " euros!");}

					Thread.sleep(800);

					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
						historyField.append("You have " + o.getPara() + " dollars left!\n");
					else if(tiles.get(0).getName().equals("Los"))
						historyField.append("Sie haben" + o.getPara() + " euros �brig!\n");
					else if(tiles.get(0).getName().equals("D�part"))
						historyField.append("Vous avez encore" + o.getPara() + " euros!\n");
				}
			}
			else {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
					boardSpeech.say("You passed " + tiles.get(o.getKonum()).getName());
					historyField.append("You passed " + tiles.get(o.getKonum()).getName() + "\n");
				}
				else if(tiles.get(0).getName().equals("Los")){
					boardSpeechde.say("Du bist vergangen " + tiles.get(o.getKonum()).getName());
					historyField.append("Du bist vergangen " + tiles.get(o.getKonum()).getName() + "\n");
				}
				else if(tiles.get(0).getName().equals("D�part")){
					boardSpeechfr.say("Tu es pass� " + tiles.get(o.getKonum()).getName());
					historyField.append("Tu es pass� " + tiles.get(o.getKonum()).getName() + "\n");
				}
			}
		}
		else if(index == 30) {
			if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
				boardSpeech.say("You had been sent to jail.");
				historyField.append("You had been sent to jail.\n");}
			else if(tiles.get(0).getName().equals("Los")){
				boardSpeechde.say("Du wurdest ins Gef�ngnis geschickt worden.");
				historyField.append("Du wurdest ins Gef�ngnis geschickt worden.\n");}
			else if(tiles.get(0).getName().equals("D�part")){
				boardSpeechfr.say("Vous aviez �t� envoy� en prison.");
				historyField.append("Vous aviez �t� envoy� en prison.\n");}
		}
		else {
			if(tiles.get(index).getClass().getName().equals("Arsa")) {

				if(((Arsa) tiles.get(index)).getOwner().getPiyon().equals(o.getPiyon())) {
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
						boardSpeech.say("You own this place. Press 1 to build. Press 2 to pass.");
					else if(tiles.get(0).getName().equals("Los"))
						boardSpeechde.say("Sie besitzen diesen Ort. Dr�cken Sie 1, um zu bauen. Dr�cken Sie 2, um zu �bergeben.");
					else if(tiles.get(0).getName().equals("D�part"))
						boardSpeechfr.say("Vous �tes propri�taire de cet endroit. Appuyez sur 1 pour cr�er. Appuyez sur 2 pour passer.");

					wait_flag = true;
					inputMode = 0;
					soundEffects("effects\\beep.mp3");
					inputField.setEnabled(true);
					inputField.requestFocusInWindow();
					while(wait_flag) {
						Thread.sleep(100);
					}
					inputField.setEnabled(false);

					if(inputMode == 1) {
						o.setPara(o.getPara() - ((Arsa) tiles.get(index)).getEv_fiyati());
						((Arsa) tiles.get(index)).build();
						if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
							boardSpeech.say("You have built a building on " + ((Arsa) tiles.get(index)).getName() + ".");
							boardSpeech.say("You have " + o.getPara() + " dollars left.");
							historyField.append("You have built a building on " + ((Arsa) tiles.get(index)).getName() + "." + 
									"You have " + o.getPara() + " dollars left." + "\n");}
						else if(tiles.get(0).getName().equals("Los")){
							boardSpeechde.say("Du hast ein Geb�ude gebaut " + ((Arsa) tiles.get(index)).getName() + ".");
							boardSpeechde.say("Sie haben " + o.getPara() + " euros �brig.");
							historyField.append("Du hast ein Geb�ude gebaut " + ((Arsa) tiles.get(index)).getName() + "." + 
									"Sie haben " + o.getPara() + " euros �brig." + "\n");}
						else if(tiles.get(0).getName().equals("D�part")){
							boardSpeechfr.say("Vous avez construit un b�timent sur " + ((Arsa) tiles.get(index)).getName() + ".");
							boardSpeechfr.say("Vous avez encore " + o.getPara() + " euros.");
							historyField.append("Vous avez construit un b�timent sur " + ((Arsa) tiles.get(index)).getName() + "." + 
									"Vous avez encore " + o.getPara() + " euros." + "\n");}
					}
					else if(inputMode == 2) {

					}
				}
				else if(((Arsa) tiles.get(index)).getOwner() != null || !((Arsa) tiles.get(index)).getOwner().getPiyon().equals(o.getPiyon())) {
					int rent = ((Arsa) tiles.get(index)).getArsa_kirasi();
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
						boardSpeech.say("You landed on " + ((Arsa) tiles.get(index)).getOwner().getPiyon() + "'s place! "
								+ "Pay your rent: " + rent + " dollars!");
					else if(tiles.get(0).getName().equals("Los"))
						boardSpeechde.say("Sie landeten am  " + ((Arsa) tiles.get(index)).getOwner().getPiyon() 
								+ "Zahlen Sie Ihre Miete: " + rent + " euros!");
					else if(tiles.get(0).getName().equals("D�part"))
						boardSpeechfr.say("Vous avez atterri sur la place de  " + ((Arsa) tiles.get(index)).getOwner().getPiyon() 
								+ "Payer votre loyer: " + rent + " euros!");

					Thread.sleep(700);

					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")) {
						historyField.append("You landed on " + ((Arsa) tiles.get(index)).getOwner().getPiyon() + "'s place! "
								+ "Pay your rent: " + rent + " dollars!\n");
						o.setPara(o.getPara() - rent);
						((Arsa) tiles.get(index)).getOwner().setPara(((Arsa) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeech.say("You got " + o.getPara() + " left.");
						historyField.append("You got " + o.getPara() + " left.\n");
						lastSpoken="you have paid " + rent + " dollars to " + ((Arsa) tiles.get(index)).getOwner().getPiyon() + ".";
					}
					else if(tiles.get(0).getName().equals("Los")) {
						historyField.append("Sie landeten am " + ((Arsa) tiles.get(index)).getOwner().getPiyon() + 
								"Zahlen Sie Ihre Miete: " + rent + " euros!\n");
						o.setPara(o.getPara() - rent);
						((Arsa) tiles.get(index)).getOwner().setPara(((Arsa) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeechde.say("Du hast " + o.getPara() + " �brig.");
						historyField.append("Du hast " + o.getPara() + " �brig.\n");
						lastSpoken_de="Sie haben  " + rent + " euros zum " + ((Arsa) tiles.get(index)).getOwner().getPiyon() + "bezahlt.";
					}
					else if(tiles.get(0).getName().equals("D�part")){
						historyField.append("Vous avez atterri sur la place de " + ((Arsa) tiles.get(index)).getOwner().getPiyon() 
								+ "Payer votre loyer: " + rent + " euros!\n");
						o.setPara(o.getPara() - rent);
						((Arsa) tiles.get(index)).getOwner().setPara(((Arsa) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeechfr.say("Vous avez encore  " + o.getPara() + " euros.");
						historyField.append("Vous avez encore " + o.getPara() + " euros.\n");
						lastSpoken_fr="Vous avez pay� " + rent + "  euros � la " + ((Arsa) tiles.get(index)).getOwner().getPiyon() + ".";
					}
				}
			}
			else if(tiles.get(index).getClass().getName().equals("Idare")) {
				if(((Idare) tiles.get(index)).getOwner() != null || !((Idare) tiles.get(index)).getOwner().getPiyon().equals(o.getPiyon())) {
					int rent = ((Idare) tiles.get(index)).kira(d);
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
						boardSpeech.say("You landed on " + ((Idare) tiles.get(index)).getOwner().getPiyon() + "'s place!"
								+ "Pay your rent: " + rent + " dollars!");
					else if(tiles.get(0).getName().equals("Los"))
						boardSpeechde.say("Sie landeten am" + ((Idare) tiles.get(index)).getOwner().getPiyon() + 
								"Zahlen Sie Ihre Miete: " + rent + " euros!");
					else if(tiles.get(0).getName().equals("D�part"))
						boardSpeechfr.say("Vous avez atterri sur la place de" + ((Idare) tiles.get(index)).getOwner().getPiyon() + 
								"Payer votre loyer: " + rent + " euros!");


					Thread.sleep(700);

					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")) {
						historyField.append("You landed on " + ((Idare) tiles.get(index)).getOwner().getPiyon() + "'s place!"
								+ "Pay your rent: " + rent + " dollars!\n");
						o.setPara(o.getPara() - rent);
						((Idare) tiles.get(index)).getOwner().setPara(((Idare) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeech.say("You got " + o.getPara() + " left.");
						historyField.append("You got " + o.getPara() + " left.\n");
						lastSpoken="you have paid " + rent + " dollars to " + ((Idare) tiles.get(index)).getOwner().getPiyon() + ".";
					}
					else if(tiles.get(0).getName().equals("Los")){
						historyField.append("Sie landeten am " + ((Idare) tiles.get(index)).getOwner().getPiyon()  
								+ "Zahlen Sie Ihre Miete: " + rent + " euros!\n");
						o.setPara(o.getPara() - rent);
						((Idare) tiles.get(index)).getOwner().setPara(((Idare) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeechde.say("Du hast " + o.getPara() + " �brig.");
						historyField.append("Du hast " + o.getPara() + " �brig.\n");
						lastSpoken_de="Sie haben " + rent + " euros zum " + ((Idare) tiles.get(index)).getOwner().getPiyon() + ".";
					}
					else if(tiles.get(0).getName().equals("D�part")){
						historyField.append("Vous avez atterri sur la place de " + ((Idare) tiles.get(index)).getOwner().getPiyon() 
								+ "Payer votre loyer: " + rent + " euros!\n");
						o.setPara(o.getPara() - rent);
						((Idare) tiles.get(index)).getOwner().setPara(((Idare) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeechfr.say("Vous avez encore " + o.getPara() + " euros.");
						historyField.append("Vous avez encore " + o.getPara() + " euros.\n");
						lastSpoken_fr="Vous avez pay� " + rent + " euros � la " + ((Idare) tiles.get(index)).getOwner().getPiyon() + ".";
					}

				}
			}
			else if(tiles.get(index).getClass().getName().equals("Istasyon")) {
				if(((Istasyon) tiles.get(index)).getOwner() != null || !((Istasyon) tiles.get(index)).getOwner().getPiyon().equals(o.getPiyon())) {
					int rent = ((Istasyon) tiles.get(index)).getKira();
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
						boardSpeech.say("You landed on " + ((Istasyon) tiles.get(index)).getOwner().getPiyon() + "'s place!"
								+ "Pay your rent: " + rent + " dollars!");
					else if(tiles.get(0).getName().equals("Los"))
						boardSpeechde.say("Sie landeten am " + ((Istasyon) tiles.get(index)).getOwner().getPiyon() 
								+ "Zahlen Sie Ihre Miete: " + rent + " euros!");
					else if(tiles.get(0).getName().equals("D�part"))
						boardSpeechfr.say("Vous avez atterri sur la place de " + ((Istasyon) tiles.get(index)).getOwner().getPiyon() 
								+ "Payer votre loyer: " + rent + " euros!");

					Thread.sleep(700);

					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
						historyField.append("You landed on " + ((Istasyon) tiles.get(index)).getOwner().getPiyon() + "'s place!"
								+ "Pay your rent: " + rent + " dollars!\n");
						o.setPara(o.getPara() - rent);
						((Istasyon) tiles.get(index)).getOwner().setPara(((Istasyon) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeech.say("You got " + o.getPara() + " left.");
						historyField.append("You got " + o.getPara() + " left.\n");
						lastSpoken="you have paid " + rent + " dollars to " + ((Istasyon) tiles.get(index)).getOwner().getPiyon() + ".";
					}
					else if(tiles.get(0).getName().equals("Los")){
						historyField.append("Sie landeten am " + ((Istasyon) tiles.get(index)).getOwner().getPiyon()
								+ "Zahlen Sie Ihre Miete: " + rent + " euros!\n");
						o.setPara(o.getPara() - rent);
						((Istasyon) tiles.get(index)).getOwner().setPara(((Istasyon) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeechde.say("Du hast " + o.getPara() + " �brig.");
						historyField.append("Du hast " + o.getPara() + " �brig.\n");
						lastSpoken_de="Sie haben " + rent + " euros zum " + ((Istasyon) tiles.get(index)).getOwner().getPiyon() + ".";
					}
					else if(tiles.get(0).getName().equals("D�part")){
						historyField.append("Vous avez atterri sur la place de " + ((Istasyon) tiles.get(index)).getOwner().getPiyon() 
								+ "Payer votre loyer: " + rent + " euros!\n");
						o.setPara(o.getPara() - rent);
						((Istasyon) tiles.get(index)).getOwner().setPara(((Istasyon) tiles.get(index)).getOwner().getPara() + rent);
						boardSpeechfr.say("Vous avez encore " + o.getPara() + " euros.");
						historyField.append("Vous avez encore " + o.getPara() + " euros.\n");
						lastSpoken_fr="Vous avez pay� " + rent + " euros � la " + ((Istasyon) tiles.get(index)).getOwner().getPiyon() + ".";
					}

				}
			}
			else if(tiles.get(index).getClass().getName().equals("Sans")) {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")) {
					boardSpeech.say(sks.getStack()[sks.getTop()].getText());
					Thread.sleep(700);
					historyField.append(sks.getStack()[sks.getTop()].getText() + "\n");
					lastSpoken = sks.getStack()[sks.getTop()].getText();
				}
				else if(tiles.get(0).getName().equals("Los")) {
					boardSpeechde.say(sks.getStack()[sks.getTop()].getText());
					Thread.sleep(700);
					historyField.append(sks.getStack()[sks.getTop()].getText() + "\n");
					lastSpoken_de = sks.getStack()[sks.getTop()].getText();
				}
				else if(tiles.get(0).getName().equals("D�part")) {
					boardSpeechfr.say(sks.getStack()[sks.getTop()].getText());
					Thread.sleep(700);
					historyField.append(sks.getStack()[sks.getTop()].getText() + "\n");
					lastSpoken_fr = sks.getStack()[sks.getTop()].getText();
				}
				sks.pop().action(o);
			}
			else if(tiles.get(index).getClass().getName().equals("Kamu_Fonu")) {
				if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")) {
					boardSpeech.say(kks.getStack()[kks.getTop()].getText());
					Thread.sleep(700);
					historyField.append(kks.getStack()[kks.getTop()].getText() + "\n");
					lastSpoken = kks.getStack()[kks.getTop()].getText();
				}
				else if(tiles.get(0).getName().equals("Los")) {
					boardSpeechde.say(kks.getStack()[kks.getTop()].getText());
					Thread.sleep(700);
					historyField.append(kks.getStack()[kks.getTop()].getText() + "\n");
					lastSpoken_de = kks.getStack()[kks.getTop()].getText();
				}
				else if(tiles.get(0).getName().equals("D�part")) {
					boardSpeechfr.say(kks.getStack()[kks.getTop()].getText());
					Thread.sleep(700);
					historyField.append(kks.getStack()[kks.getTop()].getText() + "\n");
					lastSpoken_fr = kks.getStack()[kks.getTop()].getText();
				}
				kks.pop().action(o);
			}
			else if(tiles.get(index).getClass().getName().equals("Vergi")) {
				if(index == 4) {
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")) {
						boardSpeech.say(o.getPiyon() + " has landed on Income Tax. Pay 200 dollars.");
						Thread.sleep(700);
						historyField.append(o.getPiyon() + " has landed on Income Tax. Pay 200 dollars." + "\n");
					}
					else if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Los")) {
						boardSpeechde.say(o.getPiyon() + " ist auf Einkommensteuer gelandet. Zahlen 200 euros.");
						Thread.sleep(700);
						historyField.append(o.getPiyon() + " ist auf Einkommensteuer gelandet. Zahlen 200 euros." + "\n");
					}
					else if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("D�part")) {
						boardSpeechfr.say(o.getPiyon() + " a d�barqu� sur l'imp�t sur le revenu. Payez 200 euros.");
						Thread.sleep(700);
						historyField.append(o.getPiyon() + " a d�barqu� sur l'imp�t sur le revenu. Payez 200 euros." + "\n");
					}
					o.setPara(o.getPara() - 200);
					parkMoney += 200;
				}
				else if(index == 38) {
					if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")) {
						boardSpeech.say(o.getPiyon() + " has landed on Income Tax. Pay 100 dollars.");
						Thread.sleep(700);
						historyField.append(o.getPiyon() + " has landed on Income Tax. Pay 100 dollars." + "\n");
					}
					else if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Los")) {
						boardSpeechde.say(o.getPiyon() + " ist auf Einkommensteuer gelandet. Zahlen 100 euros.");
						Thread.sleep(700);
						historyField.append(o.getPiyon() + " ist auf Einkommensteuer gelandet. Zahlen 100 euros." + "\n");
					}
					else if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("D�part")) {
						boardSpeechfr.say(o.getPiyon() + " a d�barqu� sur l'imp�t sur le revenu. Payez 100 euros.");
						Thread.sleep(700);
						historyField.append(o.getPiyon() + " a d�barqu� sur l'imp�t sur le revenu. Payez 100 euros." + "\n");
					}
					o.setPara(o.getPara() - 100);
					parkMoney += 100;
				}
			}
			else if(tiles.get(index).getClass().getName().equals("Park")) {
				if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Go")) {
					boardSpeech.say("You landed on the park. You have earned " + parkMoney + " dollars!");
					Thread.sleep(700);
					historyField.append("You landed on the park. You have earned " + parkMoney + " dollars!\n");
				}
				else if(tiles.get(0).getName().equals("Los")) {
					boardSpeechde.say("Du bist auf dem Park gelandet Du hast bekommen " + parkMoney + " euros!");
					Thread.sleep(700);
					historyField.append("Du bist auf dem Park gelandet Du hast bekommen " + parkMoney + " euros!\n");
				}
				else if(tiles.get(0).getName().equals("D�part")) {
					boardSpeechfr.say("Vous avez atterri sur le parc. Vous avez gagn� " + parkMoney + " euros!");
					Thread.sleep(700);
					historyField.append("Vous avez atterri sur le parc. Vous avez gagn� " + parkMoney + " euros!\n");
				}

				o.setPara(o.getPara() + parkMoney);
				parkMoney = 0;
			}
		}

		if(o.getPara()<=0){
			o.iflas();
			if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go")){
				boardSpeech.say(o.getPiyon()+ " has gone bankrupt!!");
				historyField.append(o.getPiyon()+ " has gone bankrupt!!\n");
			}
			else if(tiles.get(0).getName().equals("Los")){
				boardSpeechde.say(o.getPiyon()+ " Ist bankrott gegangen!!");
				historyField.append(o.getPiyon()+ " Ist bankrott gegangen!!\n");
			}
			else if(tiles.get(0).getName().equals("D�part")){
				boardSpeechfr.say(o.getPiyon()+ " A fait faillite!!");
				historyField.append(o.getPiyon()+ " A fait faillite!!\n");
			}
			o = null;
			playerNumber = playerNumber - 1;
		}

		if(tiles.get(0).getName().equals("Start") || tiles.get(0).getName().equals("Go"))
			boardSpeech.say("Press enter to end your current turn.");
		else if(tiles.get(0).getName().equals("Los"))
			boardSpeechde.say("Dr�cken Sie die Eingabetaste, um die aktuelle Runde zu beenden.");
		else if(tiles.get(0).getName().equals("D�part"))
			boardSpeechfr.say("Appuyez sur Entr�e pour terminer votre tour actuel.");
		soundEffects("effects\\beep.mp3");

		inputField.setEnabled(true);
		inputField.requestFocusInWindow();
		wait_flag = true;
		while(wait_flag) {
			Thread.sleep(100);
		}
		inputField.setEnabled(false);
		currentTurn++;
		return;
	}
	
	private void assetOverview(Oyuncu o) {
		if(tiles.get(0).getName().equals("Start")|| tiles.get(0).getName().equals("Go"))
			boardSpeech.say(o.getPiyon() + " has the following assets;");
		else if(tiles.get(0).getName().equals("Los"))
			boardSpeech.say(o.getPiyon() + " hat folgende Verm�genswerte;");
		else if(tiles.get(0).getName().equals("D�part"))
			boardSpeech.say(o.getPiyon() + " a les actifs suivants:");
		boolean cnt = false;
		for(int i = 0; i < o.getOwned_list().size(); i++) {
			if(o.getOwned_list().get(i) != null) {
				boardSpeech.say(o.getOwned_list().get(i).getName() + ", ");
				cnt = true;
			}
		}
		if(!cnt) {
			boardSpeech.say("none");
		}
	}
}	
