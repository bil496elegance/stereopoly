
public class Geri_Giden_Kamu_Karti extends Kamu_Karti{
	public Geri_Giden_Kamu_Karti(int backward, String text) {
		super();
		this.backward = backward;
		this.text = text;
	}
	public void action(Oyuncu o){
		o.git_Geri(backward);
	}
	public String getText(){
		return this.text;
	}
	private int backward;;
	private String text;
}
