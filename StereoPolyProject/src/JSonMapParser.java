import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author arda
 *
 */
public class JSonMapParser {
	public ArrayList<String> jsonParser(String mapKey) {
		JSONParser parser = new JSONParser();
		ArrayList<String> mapnames = new ArrayList<String>();
		try {

			Object obj = parser.parse(new FileReader("log\\Maps.json"));

			JSONObject jsonObject = (JSONObject) obj;
			JSONArray map = (JSONArray) jsonObject.get(mapKey);
			Iterator<String> iterator = map.iterator();

			// loop array
			while (iterator.hasNext()) {

				// System.out.println(iterator.next());
				mapnames.add((iterator.next()));

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return (mapnames);
	}
}
