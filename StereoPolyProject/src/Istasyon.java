
public class Istasyon implements Kare{
	private int fiyat;
	private Oyuncu sahip;
	private int kira;
	private int index=0;
	private boolean buyable;
	private String[] piyonlar;
	private String name;
	public Istasyon(String name, String[] piyonlar, int fiyat, int kira) {
		super();
		this.kira=4;
		this.fiyat=fiyat;
		this.name=name;
		this.buyable = true;
		this.piyonlar = piyonlar;
	}
	public String getName() {
		return name;
	}
	public Oyuncu getOwner() {
		return sahip;
	}
	public void setOwner(Oyuncu sahip) {
		this.sahip = sahip;
		hasAll();
	}
	public int getFiyat() {
		return fiyat;
	}
	public int getKira() {
		return kira;
	}
	public String[] getPiyonlar() {
		return piyonlar;
	}
	public void setPiyonlar(String[] piyonlar) {
		this.piyonlar = piyonlar;
	}
	public boolean isBuyable() {
		return buyable;
	}
	public void hasAll(){
		
		if(has(this.sahip)==2)
			this.kira=50;
		else if(has(this.sahip)==3)
			this.kira=100;
		else if(has(this.sahip)==4)
			this.kira=200;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	@Override
	public String getRenk() {
		// TODO Auto-generated method stub
		return null;
	}
	public boolean full_part(){
		//sonrnamean bak renkler ingilizce olabilir
			if(has(this.sahip)==2)
				return true;
			else
				return false;
	}
	public int has(Oyuncu o){
		if( o == null)
            return -1;
		int size=o.getOwned_list().size();
		int ret=0;
		for(int i=0;i<size;i++)
			if(o.getOwned_list().get(i).getClass().getName().equals("Istasyon"))
				ret+=1;
		return ret;
	}
	@Override
	public void setName(String s) {
		name = s;
		
	}
	@Override
	public boolean[] getOyuncuListesi() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setOyuncuListesi(boolean[] b) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setBuyable(boolean b) {
		buyable = b;
		
	}
}
