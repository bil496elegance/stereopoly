
public class Arsa implements Kare{

	public Arsa(String name, String renk, int arsa_fiyati, Oyuncu sahip, int ev_sayisi,
			boolean otel, int ev_fiyati, int otel_fiyati, boolean owned, int baseRent) {
		this.renk = renk;
		this.arsa_fiyati = arsa_fiyati;
		this.sahip = sahip;
		this.ev_sayisi = ev_sayisi;
		this.otel = otel;
		this.ev_fiyati = ev_fiyati;
		this.otel_fiyati = otel_fiyati;
		this.ipotekli_mi = false;
		this.buyable = true;
		this.owned = owned;
		arsa_kirasi = baseRent;
	}
	public String getName() {
		return name;
	}
	public void setEv_fiyati(int ev_fiyati) {
		this.ev_fiyati = ev_fiyati;
	}
	public void setOtel_fiyati(int otel_fiyati) {
		this.otel_fiyati = otel_fiyati;
	}
	public void setArsa_kirasi(int arsa_kirasi) {
		this.arsa_kirasi = arsa_kirasi;
	}
	public int has(Oyuncu o, String renk){
		int size = o.getOwned_list().size();
		int ret = 0;
		for(int i = 0 ; i < size ; i++)
			if(o.getOwned_list().get(i) == null || !o.getOwned_list().get(i).getClass().getName().equals("Arsa"))
				continue;
			else
				if(o.getOwned_list().get(i).getRenk().equals(renk))
					ret++;				
		return ret;
	}

	public void satin_al(Oyuncu o){
		this.sahip=o;
		this.buyable=false;
		if(full_part())
			this.arsa_kirasi=this.arsa_fiyati*2;
	}
	public void ipotek(){
		this.sahip.setPara(this.sahip.getPara()+this.arsa_fiyati/2);
		this.ipotekli_mi=true;
	}
	public void ipotek_kaldir(){
		this.sahip.setPara(this.sahip.getPara()-this.ipotek_kaldirma());
		this.ipotekli_mi=false;
	}

	public void setEv_sayisi(int ev_sayisi) {
		this.ev_sayisi = ev_sayisi;
	}
	public void setOtel(boolean otel) {
		this.otel = otel;
	}
	public void setIpotekli_mi(boolean ipotekli_mi) {
		this.ipotekli_mi = ipotekli_mi;
	}
	public Oyuncu getOwner() {
		return sahip;
	}
	public void setOwner(Oyuncu sahip) {
		this.sahip = sahip;
	}
	public String getRenk() {
		return renk;
	}
	public int getIndex() {
		return index;
	}


	public int getArsa_fiyati() {
		return arsa_fiyati;
	}
	public int getEv_sayisi() {
		return ev_sayisi;
	}
	public boolean isOtel() {
		return otel;
	}
	public int getEv_fiyati() {
		return ev_fiyati;
	}
	public int getOtel_fiyati() {
		return otel_fiyati;
	}
	public boolean isIpotekli_mi() {
		return ipotekli_mi;
	}
	public boolean isBuyable() {
		return buyable;
	}
	public boolean isOwned() {
		return owned;
	}
	public void setOwned(boolean owned) {
		this.owned = owned;
	}
	public String[] getPiyonlar() {
		return piyonlar;
	}


	public boolean full_part(){
		if(this.renk.equalsIgnoreCase("kahverengi")||this.renk.equalsIgnoreCase("koyu mavi")||
				this.renk.equalsIgnoreCase("brown")||this.renk.equalsIgnoreCase("blue")){
			if(has(this.sahip, this.renk)==2)
				return true;
		}else if(has(this.sahip,this.renk)==3)
			return true;
		return false;

	}
	public void setPiyonlar(String[] piyonlar) {
		this.piyonlar = piyonlar;
	}


	public double ipotek_fiyati(){
		return this.arsa_fiyati/2;
	}

	public int ipotek_kaldirma(){
		return (int)((this.arsa_fiyati/2)*1.1);
	}
	public void setIndex(int index) {
		this.index = index;
	}

	private int kiraBelirle() {
		int multiplier = 1;
		int initialRent = 0;
		if(ev_sayisi == 0) {
			initialRent = arsa_kirasi * multiplier;
		}
		else if(ev_sayisi == 1) {
			multiplier = 5;
			initialRent = arsa_kirasi * multiplier;
		}
		else if(ev_sayisi == 2) {
			multiplier = 15;
			initialRent = arsa_kirasi * multiplier;
		}
		else if(ev_sayisi == 3) {
			multiplier = 45;
			initialRent = arsa_kirasi * multiplier;
		}
		else if(ev_sayisi == 4) {
			multiplier = 80;
			initialRent = arsa_kirasi * multiplier;
		}
		else if(otel) {
			multiplier = 125;
			initialRent = arsa_kirasi * multiplier;
		}

		return initialRent;
	}

	public void build() {
		if(ev_sayisi < 4)
			ev_sayisi++;
		else if(ev_sayisi == 4) {
			ev_sayisi = 0;
			otel = true;
		}
	}

	public int getArsa_kirasi() {
		return kiraBelirle();
	}

	private String renk;
	private String name;
	private int index;
	private int arsa_fiyati;
	private Oyuncu sahip;
	private int ev_sayisi;
	private boolean otel;
	private int ev_fiyati;
	private int otel_fiyati;
	private boolean ipotekli_mi;
	private boolean buyable;
	private boolean owned;
	private String[] piyonlar;
	private int arsa_kirasi;
	private int kira;

	public int getKira() {
		return kira;
	}
	public void setKira(int kira) {
		this.kira = kira;
	}
	public void setName(String s) {
		name = s;
	}
	@Override
	public boolean[] getOyuncuListesi() {
		return null;
	}
	@Override
	public void setOyuncuListesi(boolean[] b) {

	}
	@Override
	public void setBuyable(boolean b) {
		buyable = b;
	}
	@Override
	public int getFiyat() {
		return arsa_fiyati;
	}
}
