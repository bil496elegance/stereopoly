
public class Geri_Giden_Sans_Karti implements Sans_Karti{
	public Geri_Giden_Sans_Karti(int backward, String text) {
		super();
		this.backward = backward;
		this.text = text;
	}
	public void action(Oyuncu o){
		o.git_Geri(backward);
	}
	public String getText(){
		return this.text;
	}
	private int backward;;
	private String text;
}
