
public class Geri_Kare_Giden_Sans_Karti implements Sans_Karti {
	public Geri_Kare_Giden_Sans_Karti(Kare backward, String text) {
		super();
		this.backward = backward;
		this.text = text;
	}
	public void action(Oyuncu o){
		o.git_Geri_Kare(backward);
	}
	public String getText(){
		return this.text;
	}
	private Kare backward;;
	private String text;
}
