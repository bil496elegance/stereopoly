import java.util.ArrayList;

public class Oyuncu {
	private int konum;
	private boolean[] zar_gecmisi = {false, false, false};
	private int oyuncu_no;
	private String piyon;
	private int counter = 0;
	private int zar_degeri = 0;
	private int para = 1500;
	private boolean _iflas;
	private ArrayList<Kare> owned_list = new ArrayList<Kare>(28);
	private boolean kodes;
	private boolean kodesKart�;


	public boolean getKodesKart�() {
		return kodesKart�;
	}
	public void setKodesKart�(boolean b) {
		kodesKart� = b;
	}

	public boolean isKodes() {
		return kodes;
	}

	public void setKodes(boolean kodes) {
		this.kodes = kodes;
	}

	public void iflas() {
		_iflas = true;
		for(int i = 0; i < owned_list.size(); i++){
			if(owned_list.get(i) != null){
				owned_list.get(i).setBuyable(true);
				owned_list.get(i).setOwner(null);
				if(owned_list.get(i).getClass().getName().equalsIgnoreCase("Arsa")){
					((Arsa)owned_list.get(i)).setEv_sayisi(0);
					((Arsa)owned_list.get(i)).setOtel(false);
				}
			}
		}

		if(this.kodesKart�==true)
			this.setKodesKart�(false);

		ArrayList<Kare> ownedList = new ArrayList<Kare>(28); 
		owned_list = ownedList;
	}

	public boolean isIflas() {
		return _iflas;
	}
	public void setIflas(boolean ifl) {
		_iflas = ifl;
	}
	public ArrayList<Kare> getOwned_list() {
		return owned_list;
	}

	public void setOwned_list(ArrayList<Kare> owned_list) {
		this.owned_list = owned_list;
	}

	public Oyuncu() {

	}

	public Oyuncu(String pawn) {
		piyon = pawn;
	}

	public int getOyuncu_no() {
		return oyuncu_no;
	}

	public void setOyuncu_no(int oyuncu_no) {
		this.oyuncu_no = oyuncu_no;
	}

	public int getKonum() {
		return konum;
	}

	public String getPiyon() {
		return piyon;
	}

	public int getZar_degeri() {
		return zar_degeri;
	}

	/**
	 * Returns the value of money a player holds.
	 * @return Amount of money.
	 * */
	public int getPara() {
		return para;
	}

	/**
	 * This function sets the
	 * */
	public void setPara(int para) {
		this.para = para;
	}

	private int zarTopla(int roll) {
		int z1 = roll%10;
		int z2 = roll/10;
		zar_degeri = z1 + z2;
		return z1 + z2;
	}

	/**
	 * 
	 * */
	public int zarAt() {
		int z1 = 0;
		int z2 = 0;

		z1 = 1 + (int) (Math.random() * 6);
		z2 = 1 + (int) (Math.random() * 6);

		int roll = z1 * 10 + z2;

		if (z1 == z2) {
			zar_gecmisi[counter] = true;
			counter++;
		}
		else {
			zar_gecmisi[0] = false;
			zar_gecmisi[1] = false;
			zar_gecmisi[2] = false;
			counter = 0;
		}

		if(zar_gecmisi[2]) {
			zar_gecmisi[0] = false;
			zar_gecmisi[1] = false;
			zar_gecmisi[2] = false;
			return -1;
		}

		if(this.kodes && zar_gecmisi[0]) {
			this.kodes = false;
		}

		return zarTopla(roll);
	}

	public void sat�nAl(Kare k) {
		k.setBuyable(false);
		k.setOwner(this);
		this.para -= k.getFiyat();
		owned_list.add(k);
		if(k.getClass().getName().equals("Arsa")) {
			((Arsa) k).satin_al(this);
		}
		else if(k.getClass().getName().equals("Istasyon")) {
			Istasyon temp = (Istasyon) k;
			temp.hasAll();
		}
		else if(k.getClass().getName().equals("Idare")) {
			Idare temp = (Idare) k;
			temp.hasAll();
		}
	}

	public boolean sat() {return false;}

	public int git_Ileri(int uzakl�k) {
		konum += uzakl�k;

		if(konum >= 40) { //Ba�lang��tan ge�ti
			konum = konum % 40;
			para += 200;
		}

		return konum;
	}

	public int git_Geri(int uzakl�k) {
		konum -= uzakl�k;
		return konum;
	}

	public void git_Ileri_Kare(Kare k) {
		int d = k.getIndex();
		git_Ileri(d-konum);
	}



	public void git_Geri_Kare(Kare k) {
		int d = k.getIndex();
		git_Geri(konum-d);
	}

	@Override
	public String toString() {

		return "";
	}
}
