
public class Ileri_Giden_Kamu_Karti extends Kamu_Karti{
	public Ileri_Giden_Kamu_Karti(int forward, String text) {
		super();
		this.forward = forward;
		this.text = text;
	}
	public void action(Oyuncu o){
		o.git_Ileri(forward);
	}
	public String getText(){
		return this.text;
	}
	private int forward;;
	private String text;
}
