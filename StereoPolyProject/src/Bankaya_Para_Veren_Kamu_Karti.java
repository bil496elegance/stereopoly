
public class Bankaya_Para_Veren_Kamu_Karti extends Kamu_Karti {
	public Bankaya_Para_Veren_Kamu_Karti(int para, String text) {
		super();
		this.para = para;
		this.text = text;
	}
	public void action(Oyuncu o){
		o.setPara(o.getPara()-para);
	}
	public String getText(){
		return this.text;
	}
	private int para;
	private String text;
}
