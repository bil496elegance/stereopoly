public interface Kare {
	public String getName();
	public void setName(String s);
	public int getIndex();
	public void setIndex(int i);
	public boolean [] getOyuncuListesi();
	public void setOyuncuListesi(boolean [] b);
	public void setBuyable(boolean b);
	public boolean isBuyable();
	public String getRenk();
	public void setOwner(Oyuncu pawn);
	public int getFiyat();
}
