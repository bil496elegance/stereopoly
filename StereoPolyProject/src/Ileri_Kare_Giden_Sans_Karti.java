
public class Ileri_Kare_Giden_Sans_Karti implements Sans_Karti {
	public Ileri_Kare_Giden_Sans_Karti(Kare forward, String text) {
		super();
		this.forward = forward;
		this.text = text;
	}
	public void action(Oyuncu o){
		o.git_Ileri_Kare(forward);
	}
	public String getText(){
		return this.text;
	}
	private Kare forward;;
	private String text;
}


