
public class Ileri_Giden_Sans_Karti implements Sans_Karti{
	public Ileri_Giden_Sans_Karti(int forward, String text) {
		super();
		this.forward = forward;
		this.text = text;
	}
	public void action(Oyuncu o){
		o.git_Ileri(forward);
	}
	public String getText(){
		return this.text;
	}
	private int forward;;
	private String text;
}
