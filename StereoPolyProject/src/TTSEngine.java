import java.util.Set;

import javax.sound.sampled.AudioInputStream;

import marytts.LocalMaryInterface;
import marytts.MaryInterface;
import marytts.util.data.audio.AudioPlayer;
 
public class TTSEngine {
 
	private MaryInterface marytts;
            
    public TTSEngine(String voiceName)
    {
        try
        {
        	marytts = new LocalMaryInterface();
            
            marytts.setVoice(voiceName);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void say(String input)
    {
        try
        {
        	AudioPlayer ap = new AudioPlayer();
            AudioInputStream audio = marytts.generateAudio(input);
            
            ap.setAudio(audio);
            ap.start();
            ap.join();
            
        }
        catch (Exception ex)
        {
            System.err.println("Error saying phrase.");
        }
    }

}